Rails.application.routes.draw do
  get 'home/index'
  get 'people', to: 'home#people'
  get 'mission', to: 'home#mission'
  get 'imprint', to: 'home#imprint'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
end
