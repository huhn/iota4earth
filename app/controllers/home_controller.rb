class HomeController < ApplicationController
  require 'net/http'

  def index
    uri = URI('https://ecosystem.iota.org/search_with_tags.json?tags=iota4earth&object_type=project')
    res = Net::HTTP.get(uri)
    if valid_json?(res)
      @projects = JSON.parse(res)
    else
      @people = []
    end
  end

  def people
    uri = URI('https://ecosystem.iota.org/search_with_tags.json?tags=iota4earth&object_type=user')
    res = Net::HTTP.get(uri)
    if valid_json?(res)
      @people = JSON.parse(res)
    else
      @people = []
    end
  end

  def mission

  end

  def imprint

  end

  private

  def valid_json?(string)
    begin
      !!JSON.parse(string)
    rescue JSON::ParserError
      false
    end
  end

end
